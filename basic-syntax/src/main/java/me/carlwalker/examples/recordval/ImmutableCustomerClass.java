package me.carlwalker.examples.recordval;

import java.util.Objects;

/**
 * Java Class aggregating values
 */
public class ImmutableCustomerClass {

    private final String firstName;
    private final String lastName;
    private final String city;
    private final String state;

    public ImmutableCustomerClass(String firstName, String lastName, String city, String state) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.city = city;
        this.state = state;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    // IDE generated

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ImmutableCustomerClass that = (ImmutableCustomerClass) o;
        return Objects.equals(firstName, that.firstName) && Objects.equals(lastName, that.lastName) && Objects.equals(city, that.city) && Objects.equals(state, that.state);
    }

    // IDE generated

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, city, state);
    }

    // IDE generated

    @Override
    public String toString() {
        return "CustomerClass{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                '}';
    }
}
