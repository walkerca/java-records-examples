package me.carlwalker.examples.recordval;

/**
 * Simple record example
 */
public record CustomerRecord(
        String firstName,
        String lastName,
        String city,
        String state
) {
}
