package me.carlwalker.examples.recordval;

public class LocalRecordClass {
    public String formName() {
        record Name(String firstName, String lastName) {}

        var nm = new Name("Carl", "Walker");

        return nm.lastName() + ", " + nm.firstName();
    }
}
