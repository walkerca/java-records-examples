package me.carlwalker.examples.recordval;

import java.util.Objects;

/**
 * Java Class aggregating values
 */
public class CustomerClass {

    private String firstName;
    private String lastName;
    private String city;
    private String state;

    public CustomerClass() {
    }

    public CustomerClass(String firstName, String lastName, String city, String state) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.city = city;
        this.state = state;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    // IDE generated
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CustomerClass that = (CustomerClass) o;
        return Objects.equals(firstName, that.firstName) && Objects.equals(lastName, that.lastName) && Objects.equals(city, that.city) && Objects.equals(state, that.state);
    }

    // IDE generated

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, city, state);
    }
    // IDE generated

    @Override
    public String toString() {
        return "CustomerClass{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                '}';
    }
}
