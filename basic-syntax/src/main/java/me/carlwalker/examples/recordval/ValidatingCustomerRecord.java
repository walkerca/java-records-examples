package me.carlwalker.examples.recordval;

/**
 * Simple record example
 */
public record ValidatingCustomerRecord(
        String firstName,
        String lastName,
        String city,
        String state
) {

    public ValidatingCustomerRecord {
        if( firstName == null || firstName.isBlank() ) {
            throw new IllegalArgumentException("firstName is required");
        }
        if( lastName == null || lastName.isBlank() ) {
            throw new IllegalArgumentException("lastName is required");
        }
        if( city == null || city.isBlank() ) {
            throw new IllegalArgumentException("city is required");
        }
        if( state == null || state.isBlank() ) {
            throw new IllegalArgumentException("state is required");
        }
    }
}
