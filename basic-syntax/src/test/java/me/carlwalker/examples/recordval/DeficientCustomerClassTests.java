package me.carlwalker.examples.recordval;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

/**
 * Different ways to work with Java Classes
 */
public class DeficientCustomerClassTests {

    @Test
    public void create() {
        var fn = "Carl";

        var customer = new DeficientCustomerClass(
                fn,
                "Walker",
                "Hagerstown",
                "MD"
        );

        assertEquals( fn, customer.getFirstName() );

        var sameCustomer = new DeficientCustomerClass(
                fn,
                "Walker",
                "Hagerstown",
                "MD"
        );

        // same aggregate but two different memory locations
        assertNotEquals(customer, sameCustomer);
    }
}
