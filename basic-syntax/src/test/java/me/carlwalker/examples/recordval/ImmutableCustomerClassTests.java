package me.carlwalker.examples.recordval;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Working with immutable Java Class
 */
public class ImmutableCustomerClassTests {

    @Test
    public void create() {
        var fn = "Carl";

        var customer = new ImmutableCustomerClass(
                fn,
                "Walker",
                "Hagerstown",
                "MD"
        );

        assertEquals( fn, customer.getFirstName() );
    }
}
