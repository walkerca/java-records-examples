package me.carlwalker.examples.recordval;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Different ways to work with Java Classes
 */
public class CustomerClassTests {

    @Test
    public void create() {
        var fn = "Carl";

        var customer = new CustomerClass();
        customer.setFirstName(fn);
        customer.setLastName("Walker");
        customer.setCity("Hagerstown");
        customer.setState("MD");

        assertEquals( fn, customer.getFirstName() );
    }

    @Test
    public void create2() {
        var fn = "Carl";

        var customer = new CustomerClass(
                fn,
                "Walker",
                "Hagerstown",
                "MD"
        );

        assertEquals( fn, customer.getFirstName() );

        var sameCustomer = new CustomerClass(
                fn,
                "Walker",
                "Hagerstown",
                "MD"
        );

        assertEquals(customer, sameCustomer);
    }
}
