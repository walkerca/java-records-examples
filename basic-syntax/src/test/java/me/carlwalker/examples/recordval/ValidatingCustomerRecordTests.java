package me.carlwalker.examples.recordval;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ValidatingCustomerRecordTests {

    @Test
    public void create() {
        var fn = "Carl";

        // canonical constructor
        var customer = new ValidatingCustomerRecord(
                fn,
                "Walker",
                "Hagerstown",
                "MD"
        );

        // record provides public accessor
        assertEquals( fn, customer.firstName() );
    }

    @Test
    public void firstNameRequired() {
        Exception exception = assertThrows(
                IllegalArgumentException.class,
                () ->
                new ValidatingCustomerRecord(
                null,
                "Walker",
                "Hagerstown",
                "MD"
        ));
        assertEquals("firstName is required", exception.getMessage());
    }
}
