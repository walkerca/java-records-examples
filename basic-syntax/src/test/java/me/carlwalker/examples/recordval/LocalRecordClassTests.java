package me.carlwalker.examples.recordval;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LocalRecordClassTests {

    @Test
    public void formName() {
        assertEquals("Walker, Carl", new LocalRecordClass().formName());
    }
}
