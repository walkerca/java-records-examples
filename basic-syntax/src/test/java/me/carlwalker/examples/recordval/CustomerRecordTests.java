package me.carlwalker.examples.recordval;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CustomerRecordTests {

    @Test
    public void create() {
        var fn = "Carl";

        // canonical constructor
        var customer = new CustomerRecord(
                fn,
                "Walker",
                "Hagerstown",
                "MD"
        );

        // record provides public accessor
        assertEquals( fn, customer.firstName() );

        var sameCustomer = new CustomerRecord(
                fn,
                "Walker",
                "Hagerstown",
                "MD"
        );

        assertEquals(customer, sameCustomer);
    }
}
