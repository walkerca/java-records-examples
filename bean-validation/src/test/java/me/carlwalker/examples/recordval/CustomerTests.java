package me.carlwalker.examples.recordval;

import jakarta.enterprise.inject.se.SeContainer;
import jakarta.enterprise.inject.se.SeContainerInitializer;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import jakarta.validation.Validation;
import jakarta.validation.ValidatorFactory;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CustomerTests {

    @Test
    public void create() {
        var fn = "Carl";
        var customer = new Customer(
                fn,
                "Walker",
                "Hagerstown",
                "MD"
        );
        assertEquals(fn, customer.firstName());
    }

    @Test
    public void firstNameRequired() {

        var validator = Validation
                .buildDefaultValidatorFactory()
                .getValidator();

        var customer = new Customer(
                " ",
                "Walker",
                "Hagerstown",
                "MD"
        );

        var constraintViolations =validator.validate( customer );

        assertEquals( 1, constraintViolations.size() );
        assertEquals( "firstName is required", constraintViolations.iterator().next().getMessage());
    }

    @Test
    public void cdiBad() {

        var container = createCDIContainer();

        var customerManager = container.select(CustomerManager.class);

        Customer c = new Customer(null, "Walker", "Hagerstown", "MD");

        var exception = assertThrows(ConstraintViolationException.class,
                () -> customerManager.get().formatName(c)
        );

        assertEquals("firstName is required",
                exception.getConstraintViolations().iterator().next().getMessage());
    }

    @Test
    public void cdiGood() {

        var container = createCDIContainer();

        var customerManager = container.select(CustomerManager.class);

        Customer c = new Customer("Carl", "Walker", "Hagerstown", "MD");

        assertEquals("Walker, Carl", customerManager.get().formatName(c));
    }

    private SeContainer createCDIContainer() {
        SeContainerInitializer containerInit = SeContainerInitializer.newInstance();
        SeContainer container = containerInit.initialize();
        return container;
    }
}
