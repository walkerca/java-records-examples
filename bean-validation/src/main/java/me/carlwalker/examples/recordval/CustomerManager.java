package me.carlwalker.examples.recordval;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.validation.Valid;

@ApplicationScoped
public class CustomerManager {
    public String formatName(@Valid Customer customer) {
        return customer.lastName() + ", " + customer.firstName();
    }
}
