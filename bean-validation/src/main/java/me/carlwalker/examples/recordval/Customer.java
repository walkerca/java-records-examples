package me.carlwalker.examples.recordval;

import jakarta.validation.constraints.NotBlank;

public record Customer(
        @NotBlank(message="firstName is required") String firstName,
        @NotBlank(message="lastName is required") String lastName,
        @NotBlank(message="City is required") String city,
        @NotBlank(message="State is required") String state
) {
}
